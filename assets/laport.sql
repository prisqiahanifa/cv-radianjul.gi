-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2020 at 01:10 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apsi_db05`
--

-- --------------------------------------------------------

--
-- Table structure for table `cash`
--

CREATE TABLE `cash` (
  `cash_in` int(11) NOT NULL,
  `cash_out` int(11) NOT NULL,
  `net` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash`
--

INSERT INTO `cash` (`cash_in`, `cash_out`, `net`) VALUES
(0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE `material` (
  `id_material` varchar(11) NOT NULL,
  `material_name` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`id_material`, `material_name`, `price`, `stock`) VALUES
('mat-01', 'Kaki Meja (Kayu Jati Belanda)', 5000, 0),
('mat-02', 'Papan Meja (Kayu Jati Belanda)', 10000, 0),
('mat-03', 'Akrilik', 10000, 0),
('mat-04', 'Bolt M6', 500, 0),
('mat-05', 'Nut M6', 250, 0),
('mat-06', 'Engsel', 25000, 0),
('mat-07', 'Cat (Coklat Tua)', 10000, 0),
('mat-08', 'Cat (Coklat Muda)', 10000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orderdata`
--

CREATE TABLE `orderdata` (
  `id_order` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_product` varchar(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `payment_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdata`
--

INSERT INTO `orderdata` (`id_order`, `id_user`, `id_product`, `amount`, `price`, `order_date`, `payment_status`) VALUES
(6, 5, 'lap-01', 5, 1750000, '2020-04-28', 'Waiting for payment');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id_product` varchar(11) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_product`, `product_name`, `price`, `stock`) VALUES
('lap-01', 'Lapdesk Coklat Tua', 350000, 0),
('lap-02', 'Lapdesk Coklat Muda', 350000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `recipe1`
--

CREATE TABLE `recipe1` (
  `id` varchar(11) NOT NULL,
  `needs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipe1`
--

INSERT INTO `recipe1` (`id`, `needs`) VALUES
('mat-01', 4),
('mat-02', 1),
('mat-03', 1),
('mat-04', 4),
('mat-05', 4),
('mat-06', 2),
('mat-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `recipe2`
--

CREATE TABLE `recipe2` (
  `id` varchar(11) NOT NULL,
  `needs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipe2`
--

INSERT INTO `recipe2` (`id`, `needs`) VALUES
('mat-01', 4),
('mat-02', 1),
('mat-03', 1),
('mat-04', 4),
('mat-05', 4),
('mat-06', 2),
('mat-08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `pack1` int(5) NOT NULL,
  `pack2` int(5) NOT NULL,
  `price_total` int(11) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`id`, `date`, `pack1`, `pack2`, `price_total`, `status`) VALUES
(11, '2020-04-28 17:49:42', 1, 2, 309000, 'Waiting for payment');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `name`, `phone`, `address`, `username`, `password`, `role`) VALUES
(1, 'Admin', '11', 'Bandung', 'admin', '1122', 'Admin'),
(2, 'Production', '00', 'Bandung', 'prod', '1122', 'Production'),
(4, 'Inventory', '00', 'Bandung', 'inve', '1122', 'Inventory'),
(5, 'Customer', '00', 'Bandung', 'cust', '1122', 'Customer'),
(6, 'Purchasing', '00', 'Bandung', 'purch', '1122', 'Purchasing'),
(7, 'Finance', '00', 'Bandung', 'fina', '1122', 'Finance');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id_material`);

--
-- Indexes for table `orderdata`
--
ALTER TABLE `orderdata`
  ADD PRIMARY KEY (`id_order`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`);

--
-- Indexes for table `recipe1`
--
ALTER TABLE `recipe1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recipe2`
--
ALTER TABLE `recipe2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orderdata`
--
ALTER TABLE `orderdata`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
