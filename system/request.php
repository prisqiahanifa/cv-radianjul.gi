<?php 
session_start();
include '../config/conn.php';
$pageid = $_GET['pageid'];

    if ($pageid == 1){//Production
        // activate session php
        $finid = $_GET['fin'];

        // get form data
        $qpack1 = $_POST['pack1'];
        $qpack2 = $_POST['pack2'];


        $row = $dbconnect->query("SELECT m.price, r1.needs FROM material AS m RIGHT JOIN recipe1 AS r1 ON m.id_material=r1.id");
        // get data

        $ppack1 = 0;

        if($row->rowCount() > 6) {
            while($data = $row->fetch()){
            $n = $data['needs'];
            $p = $data['price'];
            
            $ppack1 += ($n * $p);
            }
        }

        //echo $ppack1;

        $row = $dbconnect->query("SELECT m.price, r2.needs FROM material AS m RIGHT JOIN recipe2 AS r2 ON m.id_material=r2.id");
        // get data

        $ppack2 = 0;

        if($row->rowCount() > 6) {
            while($data = $row->fetch()){
            $n = $data['needs'];
            $p = $data['price'];
            
            $ppack2 += ($n * $p);
            }
        }
        //echo $ppack2;

        $tpack = ($ppack1 * $qpack1) + ($ppack2 * $qpack2);

        $row = $dbconnect->query("INSERT INTO request VALUES ('', NOW(), '$qpack1', '$qpack2', '$tpack', 'Waiting for Confirmation')") or die(mysqli_error($dbconnect));
        //echo $tpack;

        if($finid == 1){
            header("location: ../views/".$_SESSION['role']."/finance.php?page=4A&status=req");
        exit;
        }else{
            header("location: ../views/".$_SESSION['role']."/production.php?page=1A&status=req");
            exit;
        }
        
    }



    if ($pageid == 2){//Inventory
        $reqid = $_GET['id'];

        $row = $dbconnect->query("UPDATE request SET status='Waiting for payment' WHERE id='$reqid'") or die(mysqli_error($dbconnect));

        header("location: ../views/".$_SESSION['role']."/inventory.php?page=2A&status=acc");
        exit;
    }

    if ($pageid == 3){//Purchasing
        $reqid = $_GET['id'];

        $row = $dbconnect->query("UPDATE request SET status='Done' WHERE id='$reqid'") or die(mysqli_error($dbconnect));

        $row = $dbconnect->query("SELECT pack1, pack2, price_total FROM request WHERE id='$reqid'") or die(mysqli_error($dbconnect));

        $data = $row->fetch();

        $qpack1 = $data['pack1'];
        $qpack2 = $data['pack2'];
        $ptot = $data['price_total'];

        if ($qpack1 > 0) {
        $row1 = $dbconnect->query("SELECT m.material_name, m.stock, r1.needs FROM material AS m RIGHT JOIN recipe1 AS r1 ON m.id_material=r1.id") or die(mysqli_error());

            if($row1->rowCount() > 6) {
                while($data = $row1->fetch()){
                    $n = $data['needs'];
                    $q = $data['stock'];
                    $mname = $data['material_name'];

                    $q += ($qpack1 * $n);

                    $row2 = $dbconnect->query("UPDATE material SET stock='$q' WHERE material_name='$mname'") or die(mysqli_error($dbconnect));
                }
            }
        }

        if ($qpack2 > 0) {
        $row1 = $dbconnect->query("SELECT m.material_name, m.stock, r2.needs FROM material AS m RIGHT JOIN recipe2 AS r2 ON m.id_material=r2.id") or die(mysqli_error());

            if($row1->rowCount() > 6) {
                while($data = $row1->fetch()){
                    $n = $data['needs'];
                    $q = $data['stock'];
                    $mname = $data['material_name'];

                    $q += ($qpack2 * $n);

                    $row2 = $dbconnect->query("UPDATE material SET stock='$q' WHERE material_name='$mname'") or die(mysqli_error($dbconnect));
                }
            }
        }

        $row = $dbconnect->query("UPDATE cash SET cash_out=cash_out+$ptot, net=cash_in-cash_out") or die(mysqli_error($dbconnect));

        header("location: ../views/".$_SESSION['role']."/purchasing.php?page=3A&status=acc");
        exit;
    }

    if ($pageid == 4){//Finance
        $reqid = $_GET['id'];

        $row = $dbconnect->query("SELECT id_product, amount, price FROM orderdata WHERE id_order='$reqid'") or die(mysqli_error($dbconnect));

        $data = $row->fetch();

        $ordid =$data['id_product'];
        $qorder = $data['amount'];
        $stot = $data['price'];

        if ($qorder > 0) {
            $row1 = $dbconnect->query("SELECT stock FROM product WHERE id_product='$ordid'") or die(mysqli_error());

            $data = $row1->fetch();

            $q = $data['stock'];

            if ($qorder <= $q) {
                //echo $q;
                //echo $qorder;
                $q -= $qorder;

                $row2 = $dbconnect->query("UPDATE product SET stock='$q' WHERE id_product='$ordid'") or die(mysqli_error($dbconnect));
            }else{
                header("location: ../views/".$_SESSION['role']."/finance.php?page=4A&status=err");
                exit;
            }

        }
        $row = $dbconnect->query("UPDATE orderdata SET payment_status='Accepted' WHERE id_order='$reqid'") or die(mysqli_error($dbconnect));

        $row = $dbconnect->query("UPDATE cash SET cash_in=cash_in+$stot, net=cash_in-cash_out") or die(mysqli_error($dbconnect));

        header("location: ../views/".$_SESSION['role']."/finance.php?page=4A&status=acc");
        exit;
    }

?>