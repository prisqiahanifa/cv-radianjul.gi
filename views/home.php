<?php 
  include 'header.php'; 
  $row = $dbconnect->query("SELECT * FROM product");
?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <!-- TITLE -->
  <span><i class="fa fa-angle-right">&nbsp;</i>Home</span>
</div>
<!-- End of Page Heading -->


<!-- Content here -->
<div class="row justify-content-center"> 
  <div class="col-lg-10">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Item Stock</h6>
      </div>  

      <div class="card-body">
      

      <!-- TABLE HERE -->
      <div class="table-responsive">
        <table class="table" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th width="1%">No.</th>
              <th>Product Code</th>
              <th>Name</th>
              <th>Price (Rp)</th>
              <th>Stock (Unit)</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          
            <?php if($row->rowCount() > 0){
              $no = 1;
              while($data = $row->fetch()){
            ?>

            <tr>
              <td><?php echo $no ?></td>
              <td><?php echo $data['id_product'] ?></td>
              <td><?php echo $data['product_name'] ?></td>
              <td><?php echo number_format($data['price']) ?></td>
              <td><?php echo $data['stock'] ?></td>
              <td>Please Login</td>
            </tr>
            
            <?php $no++; }} ?>

          </tbody>
        </table>
      </div>
      <!-- END OF TABLE -->
      
      </div>
      
    </div>
  </div>
</div>

<!-- End of Content -->

<?php include 'footer.php' ?>