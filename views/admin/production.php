<?php 
  include 'header.php'; 
    
  if(isset($_POST['submit'])){
    if ($_POST['submit'] == 'r1') {

    $q1 = $_POST['q1'];

    $row1 = $dbconnect->query("SELECT m.material_name, m.stock, r1.needs FROM material AS m RIGHT JOIN recipe1 AS r1 ON m.id_material=r1.id WHERE stock>=$q1*needs") or die(mysqli_error());

    if($row1->rowCount() > 6) {
      while($data = $row1->fetch()){
        $n = $data['needs'];
        $q = $data['stock'];
        $mname = $data['material_name'];

        $q -= ($q1 * $n);

        $row2 = $dbconnect->query("UPDATE material SET stock='$q' WHERE material_name='$mname'") or die(mysqli_error($dbconnect));
                
      }

      $row3 = $dbconnect->query("UPDATE product SET stock=stock + $q1 WHERE id_product='lap-01'") or die(mysqli_error($dbconnect));
    
    } else{ 
?>
      <div class="alert alert-warning text-center" role="alert"><span class="badge badge-warning">Warning</span>  Not enough material, please
        <strong><a href="#" class="stretched-link"  data-toggle="modal" data-target="#purchaseModal">report purchasing</a></strong>
      </div>

<?php }
    } else if ($_POST['submit'] == 'r2') {

      $q2 = $_POST['q2'];

      $row1 = $dbconnect->query("SELECT m.material_name, m.stock, r2.needs FROM material AS m RIGHT JOIN recipe2 AS r2 ON m.id_material=r2.id WHERE stock>=$q2*needs") or die(mysqli_error());
      
      if($row1->rowCount() > 6) {
        while($data = $row1->fetch()){
          $n = $data['needs'];
          $q = $data['stock'];
          $mname = $data['material_name'];

          $q -= ($q2 * $n);

          $row2 = $dbconnect->query("UPDATE material SET stock='$q' WHERE material_name='$mname'") or die(mysqli_error($dbconnect));
        }

        $row3 = $dbconnect->query("UPDATE product SET stock=stock + $q2 WHERE id_product='lap-02'") or die(mysqli_error($dbconnect));

      } else{ 
?>
        <div class="alert alert-warning text-center" role="alert"><span class="badge badge-warning">Warning</span>  Not enough material, please
          <strong><a href="#" class="stretched-link"  data-toggle="modal" data-target="#purchaseModal">report purchasing</a></strong>
        </div>
<?php }
    }
  }

  $row = $dbconnect->query("SELECT * FROM material");
?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <!-- TITLE -->
  <span><i class="fa fa-angle-right">&nbsp;</i>Production Control</span>
</div>
<!-- End of Page Heading -->


<!-- Content here -->
<div class="row justify-content-center"> 
  <div class="col-lg-10">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Material List</h6>
      </div>  

      <div class="card-body">
      
        <!-- BUTTON-->
        <div class="pb-3"><a href="add-material.php" class="btn btn-sm btn-success"><i class="fas fa-fw fa-plus">&nbsp;</i>Add Material</a></div>
        <!-- End BUTTON -->

        <!-- TABLE HERE -->
        <div class="table-responsive">
          <table class="table table-sm" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th width="1%">No.</th>
                <th>ID Material</th>
                <th>Material Name</th>
                <th>Stock (Unit)</th>
              </tr>
            </thead>
            <tbody>
            
              <?php if($row->rowCount() > 0) {
                $no = 1;
                while($data = $row->fetch()){
              ?>


              <tr>
                  <td><?php echo $no ?></td>
                  <td><?php echo $data['id_material'] ?></td>
                  <td><?php echo $data['material_name'] ?></td>
                  <td><?php echo $data['stock'] ?></td>
              </tr>

              <?php $no++; }}?>
              
            </tbody>
          </table>
        </div>
        <!-- END OF TABLE -->
      </div>
    </div>
  </div>
</div>

<div class="row justify-content-center"> 
  <div class="col-lg-5">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Lapdesk Coklat Tua</h6>
      </div>  
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-sm text-center" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Material Name</th>
                <th>Needs</th>
              </tr>
            </thead>
            <tbody>
            
              <?php 
              $row = $dbconnect->query("SELECT m.material_name, r1.needs FROM material AS m RIGHT JOIN recipe1 AS r1 ON m.id_material=r1.id");
              if($row->rowCount() > 0) {
                while($data = $row->fetch()){
              ?>
                  
              <tr>
                  <td><?php echo $data['material_name'] ?></td>
                  <td><?php echo $data['needs'] ?></td>
              </tr>

              <?php }}?>
              
            </tbody>
          </table>
          <form method="post" action="production.php?page=1A&status=r1">
            <div class="form-group">
              <input type="number" min="0" class="form-control text-center" name="q1" placeholder="Quantity" value="0">
            </div>
            <button type="submit" name="submit" value="r1" class="btn btn-success btn-block text-uppercase">
              Produce
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-5">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Lapdesk Coklat Muda</h6>
      </div>  
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-sm text-center" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Material Name</th>
                <th>Needs</th>
              </tr>
            </thead>
            <tbody>
            
              <?php 
              $row = $dbconnect->query("SELECT m.material_name, r2.needs FROM material AS m RIGHT JOIN recipe2 AS r2 ON m.id_material=r2.id");
              if($row->rowCount() > 0) {
                while($data = $row->fetch()){
              ?>
                  
              <tr>
                  <td><?php echo $data['material_name'] ?></td>
                  <td><?php echo $data['needs'] ?></td>
              </tr>

              <?php }}?>
              
            </tbody>
          </table>
          <form method="post" action="production.php?page=1A&status=r2">
            <div class="form-group">
              <input type="number" min="0" class="form-control text-center" name="q2" placeholder="Quantity" value="0">
            </div>
            <button type="submit" name="submit" value="r2" class="btn btn-success btn-block text-uppercase">
              Produce
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Report purchasing</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form action="../../system/request.php?pageid=1" method="POST">
        <div class="modal-body">
          <div class="form-group">
            <label for="pack1" class="col-form-label">Lapdesk Coklat Tua</label>
            <input type="number" min="0" class="form-control" name="pack1" placeholder="Quantity" value="0">
          </div>
          <div class="form-group">
            <label for="pack2" class="col-form-label">Lapdesk Coklat Muda</label>
            <input type="number" min="0" class="form-control" name="pack2" placeholder="Quantity" value="0">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-warning" name="request">Request</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- End of Content -->

<?php include 'footer.php' ?>