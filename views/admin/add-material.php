<?php 
  include 'header.php'; 
  $row = $dbconnect->query("SELECT * FROM material");
?>
	
	
<!-- Page Heading -->
<div class="d-sm-flex align-Orders-center justify-content-between mb-4">
  <!-- TITLE -->
  <span><i class="fa fa-angle-right">&nbsp;</i>Add Material</span>
</div>
<!-- End of Page Heading -->


<!-- Content -->
  	
<div class="row justify-content-center"> 
  <div class="col-lg-6">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
		<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-dark">Add Material</h6>
		</div>  
		
		<form method="post" action="../../system/addmaterial.php">
			<div class="card-body">
	            <div class="form-group">
	            	<label>ID Material</label>
	            	<input type="text" class="form-control form-control-sm" name="id_material" placeholder="Input ID Material . ." required>
	        	</div>
	        	<div class="form-group">
	            	<label>Material Name</label>
	            	<input type="text" class="form-control form-control-sm" name="material_name" placeholder="Input name . ." required>
	        	</div>
	        	<div class="form-group">
	            	<label>Stock</label>
	            	<input type="number" min="0" class="form-control form-control-sm" name="stock" placeholder="Input stock . ." required>
	        	</div>
		    </div>
		    <div class="card-footer al-right">
	            <input type="submit" class="btn btn-sm btn-success" value="Add Material">
	        </div>
        </form>

    </div>
  </div>
</div>

<!-- End of Content -->

<?php include 'footer.php' ?>