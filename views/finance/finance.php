<?php 
  include 'header.php'; 

  $crow = $dbconnect->query("SELECT * FROM cash");
  $prow = $dbconnect->query("SELECT * FROM product");
  $row_od = $dbconnect->query("SELECT * FROM orderdata join user on user.id_user=orderdata.id_user");
  $reqrow = $dbconnect->query("SELECT * FROM request WHERE status='Waiting for payment' OR status='Done'");
 
  if(isset($_GET['status'])){
    if($_GET['status'] == 'err'){ 
      ?>
            <div class="alert alert-warning text-center" role="alert"><span class="badge badge-warning">Warning</span>  Not enough stock, please
              <strong><a href="#" class="stretched-link"  data-toggle="modal" data-target="#purchaseModal">report production</a></strong>
            </div>
      
      <?php }}
?>
	
	
<!-- Page Heading -->
<div class="d-sm-flex align-Orders-center justify-content-between mb-4">
  <!-- TITLE -->
  <span><i class="fa fa-angle-right">&nbsp;</i>Finance Control</span>
</div>
<!-- End of Page Heading -->


<!-- Content -->
<!-- Content here -->
<div class="row justify-content-center"> 
  <div class="col-lg-10">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Cash Flow</h6>
      </div>  

      <div class="card-body">
      
        <!-- TABLE HERE -->
        <div class="table-responsive text-center">
          <table class="table table-sm" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th width="33%">In</th>
                <th width="33%">Out</th>
                <th width="33%">Net Total</th>
              </tr>
            </thead>
            <tbody>

              <?php if($crow->rowCount() > 0) {
                while($data = $crow->fetch()){
              ?>


              <tr>
                  <td><?php echo number_format($data['cash_in']) ?></td>
                  <td>(<?php echo number_format($data['cash_out'])?>)</td>
                  <td><?php echo number_format($data['net']) ?></td>
              </tr>

              <?php }}?>
              
            </tbody>
          </table>
        </div>
        <!-- END OF TABLE -->
      </div>
    </div>
  </div>
</div> 	
<div class="row justify-content-center"> 
  <div class="col-lg-10">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Product Stock</h6>
      </div>  

      <div class="card-body">
      
        <!-- TABLE HERE -->
        <div class="table-responsive">
          <table class="table table-sm" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th width="1%">No.</th>
                <th>ID</th>
                <th>Name</th>
                <th>Stock (Unit)</th>
                <th>Price per unit (Rp)</th>
              </tr>
            </thead>
            <tbody>

              <?php if($prow->rowCount() > 0) {
                $no = 1;
                while($data = $prow->fetch()){
              ?>


              <tr>
                  <td><?php echo $no ?></td>
                  <td><?php echo $data['id_product'] ?></td>
                  <td><?php echo $data['product_name'] ?></td>
                  <td><?php echo $data['stock'] ?></td>
                  <td><?php echo number_format($data['price']) ?></td>
              </tr>

              <?php $no++; }}?>
              
            </tbody>
          </table>
        </div>
        <!-- END OF TABLE -->
      </div>
    </div>
  </div>
</div> 	
<div class="row justify-content-center"> 
  <div class="col-lg-12">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Pending Order (Cash In)</h6>
      </div>  

      <div class="card-body">

        <!-- TABLE HERE -->
        <div class="table-responsive">
          <table class="table table-sm" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>No.</th>
                <th>Ord. ID</th>
                <th>Customer</th>
                <th>Address</th>
                <th>Prod. ID</th>
                <th>Amount</th>
                <th>Price</th>
                <th>Order Date</th>
                <th>Payment Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            
              <?php if($row_od->rowCount() > 0){
                $no = 1;
                while($data = $row_od->fetch()){
              ?>

              <tr>
                <td><?php echo $no ?></td>
                <td>ORD-00<?php echo $data['id_order'] ?></td>
                <td><?php echo $data['name'] ?></td>
                <td><?php echo $data['address'] ?></td>
                <td><?php echo $data['id_product'] ?></td>
                <td><?php echo $data['amount'] ?></td>
                <td><?php echo number_format($data['price']) ?></td>
                <td><?php echo $data['order_date'] ?></td>
                <td><?php echo $data['payment_status'] ?></td>
                <td align="center">
                    <?php if ($data['payment_status'] == 'Accepted'){?>
                        - 
                    <?php }else{?>
                      <a href="../../system/request.php?id=<?php echo $data['id_order']?>&pageid=4" class="btn btn-sm btn-warning"><i class="fas fa-fw fa-check"></i></a>
                    <?php }?>
                    <a href="../../system/deleteorder.php?id=<?php echo $data['id_order']?>&pageid=3" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')"><i class="fas fa-fw fa-trash"></i></a>
                  </td>
              </tr>
              
              <?php $no++; }} ?>

            </tbody>
          </table>
        </div>
        <!-- END OF TABLE -->
      </div>
    </div>
  </div>
</div>
<div class="row justify-content-center"> 
  <div class="col-lg-12">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Request Log (Cash Out)</h6>
      </div>  

      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-sm" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th width="1%">No.</th>
                <th>ID</th>
                <th>Date</th>
                <th>Qty. lap-01</th>
                <th>Qty. lap-02</th>
                <th>Status</th>
                <th>Total (Rp)</th>
              </tr>
            </thead>
            <tbody>

              <?php if($reqrow->rowCount() > 0) {
                $no = 1;
                while($data = $reqrow->fetch()){
              ?>


              <tr>
                  <td><?php echo $no ?></td>
                  <td>REQ-00<?php echo $data['id'] ?></td>
                  <td><?php echo $data['date'] ?></td>
                  <td><?php echo $data['pack1'] ?></td>
                  <td><?php echo $data['pack2'] ?></td>
                  <td><?php echo $data['status'] ?></td>
                  <td><?php echo number_format($data['price_total']) ?></td>
              </tr>

              <?php $no++; }}?>
                         
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Report production</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form action="../../system/request.php?pageid=1&fin=1" method="POST">
        <div class="modal-body">
          <div class="form-group">
            <label for="pack1" class="col-form-label">Lapdesk Coklat Tua</label>
            <input type="number" min="0" class="form-control" name="pack1" placeholder="Quantity" value="0">
          </div>
          <div class="form-group">
            <label for="pack2" class="col-form-label">Lapdesk Coklat Muda</label>
            <input type="number" min="0" class="form-control" name="pack2" placeholder="Quantity" value="0">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-warning" name="request">Request</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- End of Content -->

<?php include 'footer.php' ?>