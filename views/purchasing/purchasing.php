<?php 
  include 'header.php'; 
  $row = $dbconnect->query("SELECT * FROM material");
  $prow = $dbconnect->query("SELECT * FROM product");

  $reqrow = $dbconnect->query("SELECT * FROM request WHERE status='Waiting for payment' OR status='Done'");
?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <!-- TITLE -->
  <span><i class="fa fa-angle-right">&nbsp;</i>Purchasing Control</span>
</div>
<!-- End of Page Heading -->


<!-- Content here -->
<div class="row justify-content-center"> 
  <div class="col-lg-10">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Inventory List</h6>
      </div>  

      <div class="card-body">
      
        <!-- TABLE HERE -->
        <div class="table-responsive">
          <table class="table table-sm" id="" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th width="1%">No.</th>
                <th>ID</th>
                <th>Name</th>
                <th>Stock (Unit)</th>
                <th>Price (Rp)</th>
              </tr>
            </thead>
            <tbody>

              <?php if($prow->rowCount() > 0) {
                $no = 1;
                while($data = $prow->fetch()){
              ?>


              <tr>
                  <td><?php echo $no ?></td>
                  <td><?php echo $data['id_product'] ?></td>
                  <td><?php echo $data['product_name'] ?></td>
                  <td><?php echo $data['stock'] ?></td>
                  <td> - </td>
              </tr>

              <?php $no++; }}?>
            
              <?php if($row->rowCount() > 0) {
                while($data = $row->fetch()){
              ?>


              <tr>
                  <td><?php echo $no ?></td>
                  <td><?php echo $data['id_material'] ?></td>
                  <td><?php echo $data['material_name'] ?></td>
                  <td><?php echo $data['stock'] ?></td>
                  <td><?php echo number_format($data['price']) ?></td>
              </tr>

              <?php $no++; }}?>
              
            </tbody>
          </table>
        </div>
        <!-- END OF TABLE -->
      </div>
    </div>
  </div>
</div>
<div class="row justify-content-center"> 
  <div class="col-lg-10">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-dark">Pending Request</h6>
      </div>  

      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-sm" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th width="1%">No.</th>
                <th>ID</th>
                <th>Date</th>
                <th>Qty. lap-01</th>
                <th>Qty. lap-02</th>
                <th>Status</th>
                <th>Total (Rp)</th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>

              <?php if($reqrow->rowCount() > 0) {
                $no = 1;
                while($data = $reqrow->fetch()){
              ?>


              <tr>
                  <td><?php echo $no ?></td>
                  <td>REQ-00<?php echo $data['id'] ?></td>
                  <td><?php echo $data['date'] ?></td>
                  <td><?php echo $data['pack1'] ?></td>
                  <td><?php echo $data['pack2'] ?></td>
                  <td><?php echo $data['status'] ?></td>
                  <td><?php echo number_format($data['price_total']) ?></td>
                  <td align="center">
                    <?php if ($data['status'] == 'Done'){?>
                        - 
                    <?php }else{?>
                      <a href="../../system/request.php?id=<?php echo $data['id']?>&pageid=3" class="btn btn-sm btn-warning"><i class="fas fa-fw fa-check"></i></a>
                    <?php }?>
                    <a href="../../system/deleteorder.php?id=<?php echo $data['id']?>&pageid=2" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')"><i class="fas fa-fw fa-trash"></i></a>
                  </td>
              </tr>

              <?php $no++; }}?>
                         
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- End of Content -->

<?php include 'footer.php' ?>